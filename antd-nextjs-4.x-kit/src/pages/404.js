import dynamic from 'next/dynamic';
import AppLoader from "../@crema/components/AppLoader";
import React from "react";

export default dynamic(() => import('../modules/errorPages/Error404'), {
  loading: () => <AppLoader />,
  ssr: false,
});
