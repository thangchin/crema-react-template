import React from "react";
import AppPage from  '@crema/core/AppLayout/AppPage';
import dynamic from 'next/dynamic';
import AppLoader from "@crema/components/AppLoader";

const ComingSoon = dynamic(() =>
  import('../../modules/errorPages/ComingSoon'),{
        loading: () => <AppLoader />,
        ssr: false,
    }
);
export default AppPage(() => <ComingSoon />);
