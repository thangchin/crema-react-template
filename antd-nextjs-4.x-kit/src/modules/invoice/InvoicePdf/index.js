import React from "react";
import { InvoicePdf } from "@crema/modules/invoice";
import { useGetDataApi } from "@crema/hooks/APIHooks";
import { useRouter } from "next/router";
import { isEmptyObject } from "@crema/helpers/ApiHelper";
import { StyledTypographyWrapper } from "../index.styled";

const InvoicePdfPage = () => {
  const { query } = useRouter();

  const [{ apiData: invoiceSettings }] = useGetDataApi(
    "/api/invoice/settings",
    {},
    {},
    true
  );

  const [{ apiData: clientsList }] = useGetDataApi(
    "/api/invoice/clients",
    {},
    {},
    true
  );

  const [{ apiData: selectedInv }] = useGetDataApi(
    "/api/invoice/detail",
    {},
    { id: query?.all?.[0] },
    true
  );

  return (
    clientsList?.length > 0 &&
    !isEmptyObject(invoiceSettings) &&
    !isEmptyObject(selectedInv) && (
      <StyledTypographyWrapper>
        <InvoicePdf
          selectedInv={selectedInv}
          clientsList={clientsList}
          invoiceSettings={invoiceSettings}
        />
      </StyledTypographyWrapper>
    )
  );
};

export default InvoicePdfPage;
