import React from 'react';
import { AddInvoice } from '@crema/modules/invoice';
import { postDataApi, useGetDataApi } from '@crema/hooks/APIHooks';
import { useRouter } from 'next/router';
import { useInfoViewActionsContext } from '@crema/context/AppContextProvider/InfoViewContextProvider';
import { StyledTypographyWrapper } from '../index.styled';

const AddInvoicePage = () => {
  const router = useRouter();
  const infoViewActionsContext = useInfoViewActionsContext();

  const [{ apiData: clientsList }] = useGetDataApi(
    '/api/invoice/clients',
    {},
    {},
    true
  );
  const [{ apiData: invoiceSettings }] = useGetDataApi(
    '/api/invoice/settings',
    {},
    {},
    true
  );

  const [{ apiData: invoiceList }] = useGetDataApi(
    '/api/invoice/list',
    {},
    {},
    true
  );
  const onSave = (invoice) => {
    postDataApi('/api/invoice/list/add', infoViewActionsContext, { invoice })
      .then(() => {
        infoViewActionsContext.showMessage(
          'New Invoice has been created successfully!'
        );
      })
      .catch((error) => {
        infoViewActionsContext.fetchError(error.message);
      });

    router.push('/invoice');
  };

  return (
    clientsList &&
    invoiceList?.length && (
      <StyledTypographyWrapper>
        <AddInvoice
          clientsList={clientsList}
          totalCount={invoiceList?.length || 0}
          invoiceSettings={invoiceSettings}
          onSave={onSave}
        />
      </StyledTypographyWrapper>
    )
  );
};

export default AddInvoicePage;
